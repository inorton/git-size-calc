#!/usr/bin/python2.7

# inspired by
# http://gregmac.net/git/2013/06/13/locating-large-objects.html

import sys
import os
import subprocess


def usage():
    helps = [word for word in sys.argv if "-h" in word]
    if len(helps):
        print "Usage: gitsize.py"
        print "Run this in a git repo"
        sys.exit(1)


# dump all the object details to a file
sys.stderr.write("Gathering Data..")
cmd = "git rev-list --objects --all | git cat-file --batch-check='%(objecttype) %(objectname) %(objectsize) %(rest)' > git.data"
subprocess.check_call(cmd, shell=True)
sys.stderr.write("..done\n")

blobs = {}
sizes = {}

# load the git.data file up and look for blobs
with open("git.data", "r") as infile:
    while True:
        line = infile.readline()
        if not line:
            break
        if line.startswith("blob"):
            words = line.rstrip().split(" ", 3)
            blobhash = words[1]
            blobsize = int(words[2])
            if blobsize > 1024:
                repofile = words[3]

                blobs[blobhash] = {
                    "file": repofile,
                }
                sizes[blobhash] = blobsize

for item in sorted(sizes, key=sizes.get, reverse=False):
    lenbytes = sizes[item]
    filename = blobs[item]["file"]
    print "{} {} {}".format(lenbytes, item, filename)
